FROM maven:3.8.4-openjdk-17-slim AS build

WORKDIR /app

# Copy mã nguồn ứng dụng Spring Boot vào thư mục /app trong container
COPY . /app

# Build ứng dụng Spring Boot với Maven (phải chạy từ thư mục chứa file pom.xml)
RUN mvn -f /app/pom.xml clean package

# Bắt đầu từ một image khác chứa OpenJDK
FROM openjdk:17

WORKDIR /app

# Copy file JAR đã được build từ stage trước vào thư mục /app trong container
COPY --from=build /app/target/spring-deploy-api-0.0.1-SNAPSHOT.jar /app/app.jar

# Cấu hình port mà ứng dụng sẽ chạy
EXPOSE 8080

# CMD để chạy ứng dụng Spring Boot khi container được khởi động
ENTRYPOINT [ "java", "-jar" , "/app/app.jar" ]